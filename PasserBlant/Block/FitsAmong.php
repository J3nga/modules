<?php
namespace Reservedel\PasserBlant\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class FitsAmong extends Template
{
    const FITS_AMONG = 'passer_blant_annet';
    /**
     * @var Product
     */
    protected $product = null;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        if (!$this->product) {
            $this->product = $this->coreRegistry->registry('product');
        }
        return $this->product;
    }
    /**
     * @param Product $product
     * @return string|null
     */
    public function getFitsAmongAttribute(Product $product): ?string
    {
        return $product->getData(self::FITS_AMONG);
    }

    /**
     * @param Product $product
     * @return string|null
     */
    public function getFitsAmongAttributeLabel(Product $product): ?string
    {
        return $product->getResource()->getAttribute(self::FITS_AMONG)->getStoreLabel();
    }
}
