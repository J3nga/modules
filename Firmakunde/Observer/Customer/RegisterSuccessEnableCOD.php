<?php

namespace Okonomideler\Firmakunde\Observer\Customer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\InputMismatchException;


class RegisterSuccessEnableCOD implements ObserverInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param  Observer $observer
     * @return RegisterSuccessEnableCOD
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     */
    public function execute(Observer $observer): RegisterSuccessEnableCOD
    {
        $customer = $observer->getEvent()->getCustomer();
        $customer->setCustomAttribute('okonomideler_allow_cash_on_delivery', 1);
        $this->customerRepository->save($customer);

        return $this;
    }
}
