<?php

declare(strict_types=1);

namespace Visma\DownloadMarketingMaterials\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\UrlInterface;
use Psr\Log\LoggerInterface;
use Visma\FrontendRestrictorForGuests\Helper\Config;

/**
 * Class Material
 * @package Visma\DownloadMarketingMaterials\Block
 */
class Material extends View
{
    const DOWNLOADBLE_FILE = 'downloadable_file_';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var FrontendRestrictor
     */
    protected $frontEndRestrictor;

    /**
     * @var DirectoryList
     */
    protected $list;
    /**
     * Material constructor.
     * @param Context $context
     * @param EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param StringUtils $string
     * @param Product $productHelper
     * @param ConfigInterface $productTypeConfig
     * @param FormatInterface $localeFormat
     * @param Session $customerSession
     * @param ProductRepositoryInterface $productRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param LoggerInterface $logger
     * @param Config $frontEndRestrictor
     * @param DirectoryList $list
     * @param array $data
     */
    public function __construct(
        Context $context,
        EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        StringUtils $string,
        Product $productHelper,
        ConfigInterface $productTypeConfig,
        FormatInterface $localeFormat,
        Session $customerSession,
        ProductRepositoryInterface $productRepository,
        PriceCurrencyInterface $priceCurrency,
        LoggerInterface $logger,
        Config $frontEndRestrictor,
        DirectoryList $list,
        array $data = []
    ) {
        $this->logger = $logger;
        $this->frontEndRestrictor = $frontEndRestrictor;
        $this->list = $list;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getMediaUrl():string
    {
        return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @param string $attachment
     * @return string
     * @throws NoSuchEntityException
     */
    public function getFullPathWithMediaUrl(string $attachment):string
    {
        return $this->getMediaUrl() . $attachment;
    }

    /**
     * @return array
     */
    public function getDownloadableAttributes():array
    {
        $list = [];
        for ($i = 1; $i <= 10; $i++) {
            $list[] = $this->getProduct()->getData(self::DOWNLOADBLE_FILE . $i);
        }
        return array_filter($list);
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedIn():bool
    {
        return $this->frontEndRestrictor->isCustomerLoggedIn();
    }

    /**
     * @param $productName
     */
    public function zipFiles($productName)
    {
        $post = $_POST;
        try {
            if (isset($post['files']) && isset($post['createzip'])) {
                $pubMediaFolder =  rtrim($this->list->getPath('media'), '/') . '/';
                if (extension_loaded('zip') && (isset($post['files']) && count($post['files']) > 0)) {
                    $zip = new \ZipArchive();
                    $zip_name = "Markedsmateriell_" . $productName . "_" . time() . ".zip";
                    $zip->open($zip_name, \ZIPARCHIVE::CREATE) === true;
                }
                foreach ($post['files'] as $file) {
                    if (!file_exists($file)
                        && is_dir($pubMediaFolder)) {
                        $zip->addFile($pubMediaFolder . $file, basename($file));
                    }
                }
                $zip->close();
                if (file_exists($zip_name)) {
                    header('Content-type: application/zip');
                    header('Content-Disposition: attachment; filename="' . $zip_name . '"');
                    while (ob_get_level()) {
                        ob_end_clean();
                    }
                    readfile($zip_name);
                    unlink($zip_name);
                }
            }
        } catch (\Exception $e) {
            $this->logger->log($e->getMessage(), ['Visma_DownloadMarketingMaterials']);
        }
    }
}
