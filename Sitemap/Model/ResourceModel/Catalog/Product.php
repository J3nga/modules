<?php

namespace Babycare\Sitemap\Model\ResourceModel\Catalog;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Gallery\ReadHandler;
use Magento\Catalog\Model\Product\Media\Config;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product as ResourceModelProduct;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\UrlInterface;
use Magento\Sitemap\Helper\Data;
use Magento\Sitemap\Model\Source\Product\Image\IncludeImage;
use Magento\Store\Model\StoreManagerInterface;

class Product extends \Magento\Sitemap\Model\ResourceModel\Catalog\Product
{
    const NOT_SELECTED_IMAGE = 'no_selection';
    /**
     * @var ReadHandler
     * @since 100.1.0
     */
    protected $mediaGalleryReadHandler;

    /**
     * Sitemap data
     *
     * @var Data
     */
    protected $_sitemapData = null;

    /**
     * @var ResourceModelProduct
     */
    protected $_productResource;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Visibility
     */
    protected $_productVisibility;

    /**
     * @var Status
     */
    protected $_productStatus;

    /**
     * @var Gallery
     * @since 100.1.0
     */
    protected $mediaGalleryResourceModel;

    /**
     * @var Config
     * @deprecated 100.2.0 unused
     */
    protected $_mediaConfig;

    /**
     * Product constructor.
     *
     * @param Context $context
     * @param Data $sitemapData
     * @param ResourceModelProduct $productResource
     * @param StoreManagerInterface $storeManager
     * @param Visibility $productVisibility
     * @param Status $productStatus
     * @param Gallery $mediaGalleryResourceModel
     * @param ReadHandler $mediaGalleryReadHandler
     * @param Config $mediaConfig
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Data $sitemapData,
        ResourceModelProduct $productResource,
        StoreManagerInterface $storeManager,
        Visibility $productVisibility,
        Status $productStatus,
        Gallery $mediaGalleryResourceModel,
        ReadHandler $mediaGalleryReadHandler,
        Config $mediaConfig
    ) {
        $this->_productResource = $productResource;
        $this->_storeManager = $storeManager;
        $this->_productVisibility = $productVisibility;
        $this->_productStatus = $productStatus;
        $this->mediaGalleryResourceModel = $mediaGalleryResourceModel;
        $this->mediaGalleryReadHandler = $mediaGalleryReadHandler;
        $this->_mediaConfig = $mediaConfig;
        $this->_sitemapData = $sitemapData;
        parent::__construct(
            $context,
            $sitemapData,
            $productResource,
            $storeManager,
            $productVisibility,
            $productStatus,
            $mediaGalleryResourceModel,
            $mediaGalleryReadHandler,
            $mediaConfig
        );
    }
    /**
     * Load product images
     *
     * @param DataObject $product
     * @param int $storeId
     * @return void
     * @throws NoSuchEntityException
     */
    protected function _loadProductImages($product, $storeId)
    {
        $this->_storeManager->setCurrentStore($storeId);
        /** @var $helper Data */
        $helper = $this->_sitemapData;
        $imageIncludePolicy = $helper->getProductImageIncludePolicy($storeId);

        // Get product images
        $imagesCollection = [];
        if (IncludeImage::INCLUDE_ALL == $imageIncludePolicy) {
            $imagesCollection = $this->_getAllProductImages($product, $storeId);
        } elseif (IncludeImage::INCLUDE_BASE == $imageIncludePolicy &&
            $product->getImage() &&
            $product->getImage() != self::NOT_SELECTED_IMAGE
        ) {
            $imagesCollection = [
                new DataObject(
                    ['url' => $this->getProductImageUrl($product->getImage())]
                ),
            ];
        }

        if ($imagesCollection) {
            // Determine thumbnail path
            $thumbnail = $product->getThumbnail();
            if ($thumbnail && $product->getThumbnail() != self::NOT_SELECTED_IMAGE) {
                try {
                    $thumbnail = $this->getProductImageUrl($thumbnail);
                } catch (NoSuchEntityException $e) {
                    $this->logger->critical($e->getMessage());
                }
            } else {
                $thumbnail = $imagesCollection[0]->getUrl();
            }
            $product->setImages(
                new DataObject(
                    ['collection' => $imagesCollection, 'title' => $product->getName(), 'thumbnail' => $thumbnail]
                )
            );
        }
    }
    /**
     * Get all product images
     *
     * @param DataObject $product
     * @param int $storeId
     * @return array
     */
    protected function _getAllProductImages($product, $storeId):?array
    {
        $product->setStoreId($storeId);
        $gallery = $this->mediaGalleryResourceModel->loadProductGalleryByAttributeId(
            $product,
            $this->mediaGalleryReadHandler->getAttribute()->getId()
        );

        $imagesCollection = [];
        if ($gallery) {
            foreach ($gallery as $image) {
                try {
                    $imagesCollection[] = new DataObject(
                        [
                            'url' => $this->getProductImageUrl($image['file']),
                            'caption' => $image['label'] ? $image['label'] : $image['label_default'],
                        ]
                    );
                } catch (NoSuchEntityException $e) {
                    $this->logger->critical($e->getMessage());
                }
            }
        }
        return $imagesCollection;
    }
    /**
     * Get product image URL without cached path from image filename
     *
     * @param string $image
     * @return string
     * @throws NoSuchEntityException
     */
    private function getProductImageUrl(string $image):string
    {
        $store = $this->_storeManager->getStore();
        return $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $image;
    }
}
