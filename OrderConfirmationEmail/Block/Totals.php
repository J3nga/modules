<?php

declare(strict_types=1);

namespace Okonomideler\OrderConfirmationEmail\Block;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;

class Totals extends Template
{
    /**
     * Associated array of totals
     * array(
     *  $totalCode => $totalObject
     * )
     *
     * @var array
     */
    protected $totals;

    /**
     * @var Order|null
     */
    protected $order = null;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * @param Context  $context
     * @param Registry $registry
     * @param array    $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize self totals and children blocks totals before html building
     *
     * @return Totals
     * @throws LocalizedException
     */
    protected function _beforeToHtml()
    {
        $this->_initTotals();
        foreach ($this->getLayout()->getChildBlocks($this->getNameInLayout()) as $child) {
            if (method_exists($child, 'initTotals') && is_callable([$child, 'initTotals'])) {
                $child->initTotals();
            }
        }
        return parent::_beforeToHtml();
    }

    /**
     * Get order object
     *
     * @return Order
     */
    public function getOrder()
    {
        if ($this->order === null) {
            if ($this->hasData('order')) {
                $this->order = $this->_getData('order');
            } elseif ($this->coreRegistry->registry('current_order')) {
                $this->order = $this->coreRegistry->registry('current_order');
            } elseif ($this->getParentBlock()->getOrder()) {
                $this->order = $this->getParentBlock()->getOrder();
            }
        }
        return $this->order;
    }

    /**
     * Sets order.
     *
     * @param  Order $order
     * @return Totals
     */
    public function setOrder($order): Totals
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get totals source object
     *
     * @return Order
     */
    public function getSource(): Order
    {
        return $this->getOrder();
    }

    /**
     * Initialize order totals array
     *
     * @return $this
     */
    protected function _initTotals(): Totals
    {
        $source = $this->getSource();

        $this->totals = [];
        $this->totals['subtotal'] = new DataObject(
            ['code' => 'subtotal', 'value' => $source->getSubtotal(), 'label' => __('Subtotal')]
        );

        /**
         * Add shipping
         */

        if (!$source->getIsVirtual() && ((double)$source->getShippingAmount() || $source->getShippingDescription())) {
            $label = __('Shipping & Handling');
            if ($this->getSource()->getCouponCode() && !isset($this->totals['discount'])) {
                $label = __('Shipping & Handling (%1)', $this->getSource()->getCouponCode());
            }

            $this->totals['shipping'] = new DataObject(
                [
                    'code' => 'shipping',
                    'field' => 'shipping_amount',
                    'value' => $this->getSource()->getShippingAmount(),
                    'label' => $label,
                ]
            );
        }

        $this->totals['grand_total'] = new DataObject(
            [
                'code' => 'grand_total',
                'field' => 'grand_total',
                'strong' => true,
                'value' => $source->getGrandTotal(),
                'label' => __('Grand Total'),
            ]
        );

        /**
         * Base grandtotal
         */
        if ($this->getOrder()->isCurrencyDifferent()) {
            $this->totals['base_grandtotal'] = new DataObject(
                [
                    'code' => 'base_grandtotal',
                    'value' => $this->getOrder()->formatBasePrice($source->getBaseGrandTotal()),
                    'label' => __('Grand Total to be Charged'),
                    'is_formated' => true,
                ]
            );
        }
        return $this;
    }

    /**
     * Add new total to totals array after specific total or before last total by default
     *
     * @param  DataObject $total
     * @param  null       $after
     * @return $this
     */
    public function addTotal(DataObject $total, $after = null): Totals
    {
        if ($after !== null && $after != 'last' && $after != 'first') {
            $totals = [];
            $added = false;
            foreach ($this->totals as $code => $item) {
                $totals[$code] = $item;
                if ($code == $after) {
                    $added = true;
                    $totals[$total->getCode()] = $total;
                }
            }
            if (!$added) {
                $last = array_pop($totals);
                $totals[$total->getCode()] = $total;
                $totals[$last->getCode()] = $last;
            }
            $this->totals = $totals;
        } elseif ($after == 'last') {
            $this->totals[$total->getCode()] = $total;
        } elseif ($after == 'first') {
            $totals = [$total->getCode() => $total];
            $this->totals = array_merge($totals, $this->totals);
        } else {
            $last = array_pop($this->totals);
            $this->totals[$total->getCode()] = $total;
            $this->totals[$last->getCode()] = $last;
        }
        return $this;
    }

    /**
     * Add new total to totals array before specific total or after first total by default
     *
     * @param  DataObject  $total
     * @param  null|string $before
     * @return $this
     */
    public function addTotalBefore(DataObject $total, $before = null): Totals
    {
        if ($before !== null) {
            if (!is_array($before)) {
                $before = [$before];
            }
            foreach ($before as $beforeTotals) {
                if (isset($this->totals[$beforeTotals])) {
                    $totals = [];
                    foreach ($this->totals as $code => $item) {
                        if ($code == $beforeTotals) {
                            $totals[$total->getCode()] = $total;
                        }
                        $totals[$code] = $item;
                    }
                    $this->totals = $totals;
                    return $this;
                }
            }
        }
        $totals = [];
        $first = array_shift($this->totals);
        $totals[$first->getCode()] = $first;
        $totals[$total->getCode()] = $total;
        foreach ($this->totals as $code => $item) {
            $totals[$code] = $item;
        }
        $this->totals = $totals;
        return $this;
    }

    /**
     * Get Total object by code
     *
     * @param string $code
     * @return false|mixed
     */
    public function getTotal(string $code)
    {
        if (isset($this->totals[$code])) {
            return $this->totals[$code];
        }
        return false;
    }

    /**
     * Delete total by specific
     *
     * @param  string $code
     * @return $this
     */
    public function removeTotal($code): Totals
    {
        unset($this->totals[$code]);
        return $this;
    }

    /**
     * Apply sort orders to totals array.
     * Array should have next structure
     * array(
     *  $totalCode => $totalSortOrder
     * )
     *
     * @param  array $order
     * @return $this
     */
    public function applySortOrder($order): Totals
    {
        \uksort(
            $this->totals,
            function ($code1, $code2) use ($order) {
                return ($order[$code1] ?? 0) <=> ($order[$code2] ?? 0);
            }
        );
        return $this;
    }

    /**
     * Get totals array for visualization
     *
     * @param  array|null $area
     * @return array
     */
    public function getTotals($area = null): array
    {
        $totals = [];
        if ($area === null) {
            $totals = $this->totals;
        } else {
            $area = (string)$area;
            foreach ($this->totals as $total) {
                $totalArea = (string)$total->getArea();
                if ($totalArea == $area) {
                    $totals[] = $total;
                }
            }
        }
        return $totals;
    }

    /**
     * Format total value based on order currency
     *
     * @param  DataObject $total
     * @return string
     */
    public function formatValue($total): string
    {
        if (!$total->getIsFormated()) {
            return $this->getOrder()->formatPrice($total->getValue());
        }
        return $total->getValue();
    }
}
