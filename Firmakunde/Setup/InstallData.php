<?php


namespace Okonomideler\Firmakunde\Setup;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    const COMPANY_NAME =  'company_name';
    const ORGANIZATION_NUMBER =  'organization_number';
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * InstallData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);


            $customerSetup->addAttribute(
                Customer::ENTITY,
                self::ORGANIZATION_NUMBER,
                [
                    'type' => 'varchar',
                    'label' => 'Organization number',
                    'input' => 'text',
                    'source' => '',
                    'required' => false,
                    'visible' => true,
                    'position' => 999,
                    'system' => false,
                    'backend' => ''
                ]
            );

            $attribute = $customerSetup->getEavConfig()->getAttribute(
               Customer::ENTITY,
                self::ORGANIZATION_NUMBER
            )
                ->addData(
                    [
                        'used_in_forms' => [
                            'adminhtml_customer',
                            'adminhtml_checkout',
                            'customer_account_create',
                            'customer_account_edit'
                        ]
                    ]
                );
            $attribute->save();


            $customerSetup->addAttribute(
              Customer::ENTITY,
                self::COMPANY_NAME,
                [
                    'type' => 'varchar',
                    'label' => 'Company Name',
                    'input' => 'text',
                    'source' => '',
                    'required' => false,
                    'visible' => true,
                    'position' => 1000,
                    'system' => false,
                    'backend' => ''
                ]
            );

            $attribute = $customerSetup->getEavConfig()->getAttribute(
              Customer::ENTITY,
               self::COMPANY_NAME
            )
                ->addData(
                    [
                        'used_in_forms' => [
                            'adminhtml_customer',
                            'adminhtml_checkout',
                            'customer_account_create',
                            'customer_account_edit'
                        ]
                    ]
                );
            $attribute->save();
        }
}
