<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Okonomideler_OrderConfirmationEmail', __DIR__);
