<?php

declare(strict_types=1);

namespace Babycare\ShowOutOfStockProduct\Model\ElasticSearch;

use Magento\CatalogInventory\Model\Configuration;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use Smile\ElasticsuiteCore\Api\Search\ContextInterface;
use Smile\ElasticsuiteCore\Api\Search\Request\Container\FilterInterface;
use Smile\ElasticsuiteCore\Search\Request\Query\QueryFactory;
use Smile\ElasticsuiteCore\Search\Request\QueryInterface;

class StockFilter implements FilterInterface
{
    /**
     * @var QueryFactory
     */
    private $queryFactory;

    /**
     * @param Registry $registry
     */
    protected $registry;

    /**
     * @var ContextInterface
     */
    private $searchContext;

    /**
     * @param QueryFactory $queryFactory
     * @param Configuration $stockConfiguration
     * @param RequestInterface $request
     * @param ContextInterface $searchContext
     * @param Registry $registry
     */
    public function __construct(
        QueryFactory $queryFactory,
        Configuration $stockConfiguration,
        RequestInterface $request,
        ContextInterface $searchContext,
        Registry $registry
    ) {
        $this->queryFactory = $queryFactory;
        $this->stockConfiguration = $stockConfiguration;
        $this->request = $request;
        $this->registry = $registry;
        $this->searchContext = $searchContext;
    }
    /**
     * {@inheritdoc}
     */
    public function getFilterQuery()
    {
        if (true === $this->isEnabledShowOutOfStock($this->searchContext->getStoreId())) {

            return $this->queryFactory->create(
                QueryInterface::TYPE_BOOL,
                [
                    'should' => [
                        $this->queryFactory->create(
                            QueryInterface::TYPE_TERM,
                            ['field' => 'show_product_in_catalog', 'value' => true]
                        ),
                        $this->queryFactory->create(
                            QueryInterface::TYPE_TERM,
                            ['field' => 'stock.is_in_stock', 'value' => true]
                        )
                    ]
                ]
            );
        }
    }

    /**
     * Get config value for 'display out of stock' option
     *
     * @param int $storeId The Store Id
     *
     * @return bool
     */
    private function isEnabledShowOutOfStock($storeId = null)
    {
        return $this->stockConfiguration->isShowOutOfStock($storeId);
    }
}
