<?php

namespace Okonomideler\BackOrder\ViewModel;

use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use \Magento\CatalogInventory\Api\StockStateInterface;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;

class BackOrderInfo extends AbstractHelper implements ArgumentInterface
{
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var StockStateInterface
     */
    protected $stockState;
    /**
     * @var GetProductSalableQtyInterface
     */
    protected $stockSalableQty;

    /**
     * BackOrderInfo constructor.
     *
     * @param StockRegistryInterface        $stockRegistry
     * @param StockStateInterface           $stockState
     * @param GetProductSalableQtyInterface $stockSalableQty
     */
    public function __construct(
        StockRegistryInterface $stockRegistry,
        StockStateInterface $stockState,
        GetProductSalableQtyInterface $stockSalableQty
    ) {
        $this->stockRegistry = $stockRegistry;
        $this->stockSalableQty = $stockSalableQty;
    }

    /**
     * @param  $productId
     * @return StockItemInterface
     */
    public function getStockItem($productId): StockItemInterface
    {
        return $this->stockRegistry->getStockItem($productId);
    }

    /**
     * @param  $productId
     * @return int
     */
    public function getStockId($productId): int
    {
        return $this->getStockItem($productId)->getStockId();
    }

    /**
     * @param  $sku
     * @param  $productId
     * @return float
     * @throws InputException
     * @throws LocalizedException
     */
    public function getSalableQty($sku, $productId): float
    {
        return $this->stockSalableQty->execute($sku, $this->getStockId($productId));
    }

    /**
     * @param  $salableQty
     * @param  $cartItemQty
     * @return int
     */
    public function getBackorders($salableQty, $cartItemQty): int
    {
        if($salableQty <= 0){
            return $cartItemQty;
        }
        elseif ($salableQty > $cartItemQty){
            return 0;
        }
        return abs($salableQty - $cartItemQty);
    }

}
