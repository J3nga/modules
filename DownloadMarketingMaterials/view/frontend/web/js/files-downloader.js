/**
 * @author aurimas.t@emotion.lt
 */

define(['jquery'], function ($) {
    'use strict';

    $.widget('visma.filesDownloader', {
        options: {
            downloadSelectedBtn: '[data-role=selected]',
            downloadAllBtn: '[data-role=all]',
            controls: 'input[type=checkbox]'
        },

        /**
         * @inheritDoc
         * @private
         */
        _create: function () {
            this._initElements()
                ._bind();
        },

        /**
         * @return {widget}
         * @private
         */
        _initElements: function () {
            this.form = this.element;

            this.controls = this.form.find(this.options.controls);
            this.downloadSelectedBtn = this.form.find(this.options.downloadSelectedBtn);
            this.downloadAllBtn = this.form.find(this.options.downloadAllBtn);

            this._disableButton(this.downloadSelectedBtn);

            return this;
        },

        /**
         * @return {widget}
         * @private
         */
        _bind: function () {
            this.controls.on('change', this._manageButtons.bind(this));
            this.downloadAllBtn.on('click', this._onDownloadAll.bind(this));

            return this;
        },

        /**
         * @return {widget}
         * @private
         */
        _manageButtons: function () {
            let isAnyControlSelected = false;

            $.each(this.controls, function (_, control) {
                if ($(control).is(':checked')) {
                    isAnyControlSelected = true;
                }
            });

            if (isAnyControlSelected) {
                this._enableButton(this.downloadSelectedBtn);
            } else {
                this._disableButton(this.downloadSelectedBtn);
            }

            return this;
        },

        /**
         * @private
         */
        _onDownloadAll: function () {
            $.each(this.controls, function (_, control) {
                $(control).prop('checked', true);
            });

            this._manageButtons();
        },

        /**
         * @param {jQuery} $button
         * @return {widget}
         * @private
         */
        _disableButton: function ($button) {
            $button.prop('disabled', true);

            return this;
        },

        /**
         * @param {jQuery} $button
         * @return {widget}
         * @private
         */
        _enableButton: function ($button) {
            $button.prop('disabled', false);

            return this;
        }
    });

    return $.visma.filesDownloader;
});
