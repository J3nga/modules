<?php

declare(strict_types=1);

namespace Babycare\ShowOutOfStockProduct\Model\ElasticSearch;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Smile\ElasticsuiteCore\Api\Index\DatasourceInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterface;

class ShowOutOfStockProduct implements DatasourceInterface
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * HiddenProductDataSource constructor.
     * @param CollectionFactory $productCollectionFactory
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        StockRegistryInterface $stockRegistry
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @param int $storeId
     * @param array $indexData
     * @return array
     */
    public function addData($storeId, array $indexData)
    {
        $collection = $this->productCollectionFactory->create()
            ->addAttributeToSelect('show_product_in_catalog_setting', 1)
            ->addFieldToFilter('entity_id', ['in' => array_keys($indexData)]);
        $items = $collection->getItems();

        if (!empty($items)) {
            foreach ($indexData as $productId => &$value) {
                $showProductInCatalogSetting = $items[$productId]->getData('show_product_in_catalog_setting');
                if ($showProductInCatalogSetting) {
                    $value['show_product_in_catalog'] = true;
                } else {
                    $value['show_product_in_catalog'] = false;
                }
            }
        }

        return $indexData;
    }
}
