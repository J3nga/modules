<?php

namespace Okonomideler\OrderConfirmationEmail\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\Session;

class OrderEmailTemplate implements ObserverInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * OrderEmailTemplate constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        Session $customerSession
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        if ($this->customerSession->isLoggedIn()) {
            try {
                $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
            } catch (NoSuchEntityException $e) {
                throw new \Exception($e->getMessage());
            }

            if (isset($customer)) {
                $customerCustomAttributes = $customer->getCustomAttributes();
                $transport = $observer->getTransport();

                if (!isset($customerCustomAttributes['navimage_external_customer_id'])) {
                    $transport['navimage_external_customer_id'] = '--';
                    return;
                }
                $customerNumber = $customerCustomAttributes['navimage_external_customer_id'];
                $customerNumberValue = $customerNumber->getValue();
                $transport['navimage_external_customer_id'] = $customerNumberValue;
            }
        }
    }
}
