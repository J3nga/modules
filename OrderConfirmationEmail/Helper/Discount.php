<?php

namespace Okonomideler\OrderConfirmationEmail\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Discount extends AbstractHelper
{
    /**
     * @param $discountAmount
     * @param $itemPrice
     * @param $orderedQty
     * @return string
     */
    public function convertDiscountAmountToPercentage($discountAmount, $itemPrice, $orderedQty)
    {
        return round(($discountAmount/$itemPrice)*100) / $orderedQty . '%';
    }
}
