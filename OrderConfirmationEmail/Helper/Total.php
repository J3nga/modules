<?php

namespace Okonomideler\OrderConfirmationEmail\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Total extends AbstractHelper
{
    /**
     * @param $itemPrice
     * @param $itemDiscount
     * @param $itemTax
     * @return mixed
     */
    public function itemTotalPrice($itemPrice, $itemDiscount, $itemTax)
    {
        return $itemPrice - $itemDiscount + $itemTax;
    }

}
